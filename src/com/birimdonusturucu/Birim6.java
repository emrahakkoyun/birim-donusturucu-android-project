package com.birimdonusturucu;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class Birim6 extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_birim6);
	    //Burda AdView objesini olu�turuyoruz ve anasayfa.xml de olu�turdu�umuz adView e ba�l�yoruz
	    AdView adView = (AdView) this.findViewById(R.id.adView1);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest); //adView i y�kl�yoruz
		final TextView text = (TextView)findViewById(R.id.sonuc);
		final Button btn = (Button)findViewById(R.id.button1);
		final EditText et = (EditText)findViewById(R.id.editText1);
		Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
		final Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
		String[] content1 = { "Y�l", "Ay", "Hafta", "G�n", "Saat", "Dakika", "Saniye"  };
		String[] content2 = {"Y�l", "Ay", "Hafta", "G�n", "Saat", "Dakika", "Saniye"   };
		ArrayAdapter adapter1 = new ArrayAdapter(this,
		android.R.layout.simple_list_item_1, content1);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_list_item_1, content2);
		adapter1.setDropDownViewResource(R.layout.spinner_layout);
		adapter2.setDropDownViewResource(R.layout.spinner_layout);
		spinner1.setAdapter(adapter1);
		spinner2.setAdapter(adapter2);
        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				// TODO Auto-generated method stub
				String selectedItem = parent.getItemAtPosition(pos).toString();
				if(selectedItem=="Y�l")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Y�l")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Ay")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*12;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Hafta")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*(52.18);
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="G�n")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*(365.25);
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saat")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*(8766);
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Dakika")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*(525960);
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saniye")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*(31557600);
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
				if(selectedItem=="Ay")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Y�l")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2 = no2*0.08;
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Ay")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Hafta")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*4.35;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="G�n")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*30.44;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saat")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*730.5;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Dakika")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*43830;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saniye")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*2629800;
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
				if(selectedItem=="Hafta")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Y�l")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*0.02;
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Ay")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*0.23;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Hafta")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="G�n")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*7;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saat")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*168;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Dakika")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*10080;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saniye")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*604800.02;
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
				if(selectedItem=="G�n")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Y�l")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*(2.74/1000);
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Ay")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*0.03;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Hafta")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*0.14;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="G�n")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saat")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*24;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Dakika")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*1440;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saniye")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*86400;
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
				if(selectedItem=="Saat")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Y�l")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*((1.14)/1000);
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Ay")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*((1.37)/1000);
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Hafta")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*0.01;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="G�n")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*0.04;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saat")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Dakika")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*60;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saniye")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*3600;
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
				
				if(selectedItem=="Dakika")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Y�l")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*((1.9)/1000000);
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Ay")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*((2.28)/10000);
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Hafta")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*((9.92)/100000);
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="G�n")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*((6.94)/10000);
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saat")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*(0.02);
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Dakika")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saniye")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no);
                                             no2=no2*60;
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}if(selectedItem=="Saniye")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Y�l")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*((3.17)/100000000);
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Ay")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*(3.8)/10000000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Hafta")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*((1.65)*1000000);
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="G�n")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*((1.16)/100000);
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saat")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*((2.78)/10000);
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Dakika")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*0.02;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Saniye")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no);
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
			}
 
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
 
			}
		});
        
	}
}
