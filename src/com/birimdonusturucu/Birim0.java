package com.birimdonusturucu;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class Birim0 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_birim0);
	    //Burda AdView objesini oluşturuyoruz ve anasayfa.xml de oluşturduğumuz adView e bağlıyoruz
	    AdView adView = (AdView) this.findViewById(R.id.adView1);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest); //adView i yüklüyoruz
		final TextView text = (TextView)findViewById(R.id.sonuc);
		final Button btn = (Button)findViewById(R.id.button1);
		final EditText et = (EditText)findViewById(R.id.editText1);
		Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
		final Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
		String[] content1 = { "Celcius", "Fahrenhayt", "Kelvin", "Santigrat", "Reaumur" };
		String[] content2 = { "Celcius", "Fahrenhayt", "Kelvin", "Santigrat", "Reaumur" };
		ArrayAdapter adapter1 = new ArrayAdapter(this,
		android.R.layout.simple_list_item_1, content1);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_list_item_1, content2);
		adapter1.setDropDownViewResource(R.layout.spinner_layout);
		adapter2.setDropDownViewResource(R.layout.spinner_layout);
		spinner1.setAdapter(adapter1);
		spinner2.setAdapter(adapter2);
        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				// TODO Auto-generated method stub
				String selectedItem = parent.getItemAtPosition(pos).toString();
				if(selectedItem=="Celcius")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Celcius")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Fahrenhayt")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*1.8+32;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Kelvin")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2+273.15;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Santigrat")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Reaumur")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=(no2*8)/10;
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
				if(selectedItem=="Fahrenhayt")
				{
//////////////////////////////////////
	 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				String selectedItem2 = parent.getItemAtPosition(pos).toString();
				if(selectedItem2=="Celcius")
				{ 
					
					btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=(100*(no2-32))/180;
						        text.setText(String.valueOf(no2));
						}
					});
					
				}
				if(selectedItem2=="Fahrenhayt")
				{ 
                      btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="Kelvin")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=((100*(no2-32))/180)+273;
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="Santigrat")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=(100*(no2-32))/180;
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="Reaumur")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=(80*(no2-32))/180;
						        text.setText(String.valueOf(no2));
						}
					});
				}
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
 
			}
	 });
	
				}
				if(selectedItem=="Kelvin")
				{
//////////////////////////////////////
	 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				String selectedItem2 = parent.getItemAtPosition(pos).toString();
				if(selectedItem2=="Celcius")
				{ 
					
					btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2-273;
						        text.setText(String.valueOf(no2));
						}
					});
					
				}
				if(selectedItem2=="Fahrenhayt")
				{ 
                      btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=((180*(no2-273))/100)+32;
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="Kelvin")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="Santigrat")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2-273;
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="Reaumur")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=(80*(no2-273))/100;
						        text.setText(String.valueOf(no2));
						}
					});
				}
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
 
			}
	 });
	
				}
				if(selectedItem=="Santigrat")
				{
//////////////////////////////////////
	 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				String selectedItem2 = parent.getItemAtPosition(pos).toString();
				if(selectedItem2=="Celcius")
				{ 
					
					btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
						        text.setText(String.valueOf(no2));
						}
					});
					
				}
				if(selectedItem2=="Fahrenhayt")
				{ 
                      btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=((180*(no2))/100)+32;
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="Kelvin")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2+273;
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="Santigrat")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="Reaumur")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=(80*(no2))/100;
						        text.setText(String.valueOf(no2));
						}
					});
				}
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
 
			}
	 });
	
				}
				if(selectedItem=="Reaumur")
				{
//////////////////////////////////////
	 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				String selectedItem2 = parent.getItemAtPosition(pos).toString();
				if(selectedItem2=="Celcius")
				{ 
					
					btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=(100*(no2))/80;
						        text.setText(String.valueOf(no2));
						}
					});
					
				}
				if(selectedItem2=="Fahrenhayt")
				{ 
                      btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=((180*(no2))/80)+32;
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="Kelvin")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=((100*(no2))/80)+273;
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="Santigrat")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=(100*(no2))/80;
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="Reaumur")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
						        text.setText(String.valueOf(no2));
						}
					});
				}
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
 
			}
	 });
	
				}
			}
 
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
 
			}
		});
	}
}
