package com.birimdonusturucu;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class Birim3 extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_birim3);
	    //Burda AdView objesini olu�turuyoruz ve anasayfa.xml de olu�turdu�umuz adView e ba�l�yoruz
	    AdView adView = (AdView) this.findViewById(R.id.adView1);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest); //adView i y�kl�yoruz
		final TextView text = (TextView)findViewById(R.id.sonuc);
		final Button btn = (Button)findViewById(R.id.button1);
		final EditText et = (EditText)findViewById(R.id.editText1);
		Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
		final Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
		String[] content1 = { "km/s", "m/s", "km/saat", "mm/s", "�m/s" };
		String[] content2 = { "km/s", "m/s", "km/saat", "mm/s", "�m/s"  };
		ArrayAdapter adapter1 = new ArrayAdapter(this,
		android.R.layout.simple_list_item_1, content1);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_list_item_1, content2);
		adapter1.setDropDownViewResource(R.layout.spinner_layout);
		adapter2.setDropDownViewResource(R.layout.spinner_layout);
		spinner1.setAdapter(adapter1);
		spinner2.setAdapter(adapter2);
        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				// TODO Auto-generated method stub
				String selectedItem = parent.getItemAtPosition(pos).toString();
				if(selectedItem=="km/s")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="km/s")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="m/s")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*1000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="km/saat")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*3600;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="mm/s")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*1000000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="�m/s")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*1000000000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
				if(selectedItem=="m/s")
				{
//////////////////////////////////////
	 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				String selectedItem2 = parent.getItemAtPosition(pos).toString();
				if(selectedItem2=="km/s")
				{ 
					
					btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2/1000;
						        text.setText(String.valueOf(no2));
						}
					});
					
				}
				if(selectedItem2=="m/s")
				{ 
                      btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="km/saat")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2*(36/10);
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="mm/s")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2*1000;
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="�m/s")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2*1000000;
						        text.setText(String.valueOf(no2));
						}
					});
				}
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
 
			}
	 });
	
				}
				if(selectedItem=="km/saat")
				{
//////////////////////////////////////
	 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				String selectedItem2 = parent.getItemAtPosition(pos).toString();
				if(selectedItem2=="km/s")
				{ 
					
					btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2*((278/100)/10000);
						        text.setText(String.valueOf(no2));
						}
					});
					
				}
				if(selectedItem2=="m/s")
				{ 
                      btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2*(28/100);
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="km/saat")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="mm/s")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2*(27778/100);
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="�m/s")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2*(27777778/100);
						        text.setText(String.valueOf(no2));
						}
					});
				}
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
 
			}
	 });
	
				}
				if(selectedItem=="mm/s")
				{
//////////////////////////////////////
	 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				String selectedItem2 = parent.getItemAtPosition(pos).toString();
				if(selectedItem2=="km/s")
				{ 
					
					btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2/1000000;
						        text.setText(String.valueOf(no2));
						}
					});
					
				}
				if(selectedItem2=="m/s")
				{ 
                      btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2/1000;
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="km/saat")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2*((36/10)/1000);
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="mm/s")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="�m/s")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2*1000;
						        text.setText(String.valueOf(no2));
						}
					});
				}
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
 
			}
	 });
	
				}
				if(selectedItem=="�m/s")
				{
//////////////////////////////////////
	 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				String selectedItem2 = parent.getItemAtPosition(pos).toString();
				if(selectedItem2=="km/s")
				{ 
					
					btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2/1000000000;
						        text.setText(String.valueOf(no2));
						}
					});
					
				}
				if(selectedItem2=="m/s")
				{ 
                      btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2/1000000;
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="km/saat")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2*((36/10)/1000000);
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="mm/s")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
							  no2=no2/1000;
						        text.setText(String.valueOf(no2));
						}
					});
				}
				if(selectedItem2=="�m/s")
				{ 
btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 String no = et.getText().toString();
							  Double no2=Double.parseDouble(no); 
						        text.setText(String.valueOf(no2));
						}
					});
				}
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
 
			}
	 });
	
				}
			}
 
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
 
			}
		});
	}
}
