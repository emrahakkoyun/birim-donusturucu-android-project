package com.birimdonusturucu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	    //Burda AdView objesini oluşturuyoruz ve anasayfa.xml de oluşturduğumuz adView e bağlıyoruz
	    AdView adView = (AdView) this.findViewById(R.id.adView1);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest); //adView i yüklüyoruz
		GridView gridView = (GridView) findViewById(R.id.grid);
		 
        // Instance of ImageAdapter Class
        gridView.setAdapter(new ImageAdapter(this));
 
        /**
         * On Click event for Single Gridview Item
         * */
        gridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                    int position, long id) {
            	   Intent i = getIntent();
                   if(position==0)
                   {
                   	Intent intent = new Intent(MainActivity.this,Birim0.class);
                   	startActivity(intent);
                   }
                   if(position==1)
                   {
                   	Intent intent = new Intent(MainActivity.this,Birim1.class);
                   	startActivity(intent);
                   }
                   if(position==2)
                   {
                   	Intent intent = new Intent(MainActivity.this,Birim2.class);
                   	startActivity(intent);
                   }
                   if(position==3)
                   {
                   	Intent intent = new Intent(MainActivity.this,Birim3.class);
                   	startActivity(intent);
                   }
                   if(position==4)
                   {
                   	Intent intent = new Intent(MainActivity.this,Birim4.class);
                   	startActivity(intent);
                   }
                   if(position==5)
                   {
                   	Intent intent = new Intent(MainActivity.this,Birim5.class);
                   	startActivity(intent);
                   }
                   if(position==6)
                   {
                   	Intent intent = new Intent(MainActivity.this,Birim6.class);
                   	startActivity(intent);
                   }
                   if(position==7)
                   {
                   	Intent intent = new Intent(MainActivity.this,Birim7.class);
                   	startActivity(intent);
                   }
                // passing array index
                i.putExtra("id", position);
                startActivity(i);
            }
        });
	}
}
