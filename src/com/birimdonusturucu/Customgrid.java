package com.birimdonusturucu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
 
public class Customgrid extends Activity {
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
        super.onCreate(savedInstanceState);
        // get intent data
        Intent i = getIntent();
        
        // Selected image id
        int position = i.getExtras().getInt("id");
        if(position==0)
        {
        	Intent intent = new Intent(Customgrid.this,Birim0.class);
        	startActivity(intent);
        }
        if(position==1)
        {
        	Intent intent = new Intent(Customgrid.this,Birim1.class);
        	startActivity(intent);
        }
        if(position==2)
        {
        	Intent intent = new Intent(Customgrid.this,Birim2.class);
        	startActivity(intent);
        }
        if(position==3)
        {
        	Intent intent = new Intent(Customgrid.this,Birim3.class);
        	startActivity(intent);
        }
        if(position==4)
        {
        	Intent intent = new Intent(Customgrid.this,Birim4.class);
        	startActivity(intent);
        }
        if(position==5)
        {
        	Intent intent = new Intent(Customgrid.this,Birim5.class);
        	startActivity(intent);
        }
        if(position==6)
        {
        	Intent intent = new Intent(Customgrid.this,Birim6.class);
        	startActivity(intent);
        }
        if(position==7)
        {
        	Intent intent = new Intent(Customgrid.this,Birim7.class);
        	startActivity(intent);
        }
    }
 
}