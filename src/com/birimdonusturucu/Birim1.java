package com.birimdonusturucu;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;


public class Birim1 extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_birim1);
	    //Burda AdView objesini oluşturuyoruz ve anasayfa.xml de oluşturduğumuz adView e bağlıyoruz
	    AdView adView = (AdView) this.findViewById(R.id.adView1);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest); //adView i yüklüyoruz
		final TextView text = (TextView)findViewById(R.id.sonuc);
		final Button btn = (Button)findViewById(R.id.button1);
		final EditText et = (EditText)findViewById(R.id.editText1);
		Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
		final Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
		String[] content1 = { "Kilometre", "Hektometre", "Dekametre", "Metre","Desimetre", "Santimetre", "Milimetre" };
		String[] content2 = { "Kilometre", "Hektometre", "Dekametre", "Metre","Desimetre", "Santimetre", "Milimetre" };
		ArrayAdapter adapter1 = new ArrayAdapter(this,
		android.R.layout.simple_list_item_1, content1);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_list_item_1, content2);
		adapter1.setDropDownViewResource(R.layout.spinner_layout);
		adapter2.setDropDownViewResource(R.layout.spinner_layout);
		spinner1.setAdapter(adapter1);
		spinner2.setAdapter(adapter2);
        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				// TODO Auto-generated method stub
				String selectedItem = parent.getItemAtPosition(pos).toString();
				if(selectedItem=="Kilometre")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Kilometre")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Hektometre")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*10;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Dekametre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*100;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Metre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*1000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Desimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*10000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Santimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*1000000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Milimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*10000000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
				if(selectedItem=="Hektometre")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Kilometre")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2 = no2/10;
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Hektometre")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Dekametre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*10;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Metre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*100;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Desimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*1000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Santimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*10000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Milimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2*100000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
				if(selectedItem=="Dekametre")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Kilometre")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/100;
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Hektometre")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/10;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Dekametre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Metre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/0.1;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Desimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/0.01;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Santimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/0.001;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Milimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/0.0001;
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
				if(selectedItem=="Metre")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Kilometre")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/1000;
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Hektometre")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/100;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Dekametre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/10;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Metre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Desimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/0.1;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Santimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/0.01;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Milimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/0.001;
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
				if(selectedItem=="Santimetre")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Kilometre")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/10000;
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Hektometre")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/1000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Dekametre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/100;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Metre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/10;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Desimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Santimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/0.1;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Milimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/0.01;
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
				if(selectedItem=="Milimetre")
				{ 
					//////////////////////////////////////
					 spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view,
									int pos, long id) {
								String selectedItem2 = parent.getItemAtPosition(pos).toString();
								if(selectedItem2=="Kilometre")
								{ 
									
									btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/1000000;
										        text.setText(String.valueOf(no2));
										}
									});
									
								}
								if(selectedItem2=="Hektometre")
								{ 
                                      btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/100000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Dekametre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/10000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Metre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/1000;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Desimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/100;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Santimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no); 
											  no2=no2/10;
										        text.setText(String.valueOf(no2));
										}
									});
								}
								if(selectedItem2=="Milimetre")
								{ 
btn.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											 String no = et.getText().toString();
											  Double no2=Double.parseDouble(no);
										        text.setText(String.valueOf(no2));
										}
									});
								}
							}
							
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
				 
							}
					 });
					
				}
			}
 
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
 
			}
		});
	}
}
